import numpy as np
from math import sin, cos
import cv2

DEGREE_TO_RADIAN_FACTOR = 0.0174533

points_image_ori = []

theta = 0.0
tx = 0.0
ty = 0.0

epsilonTx = 2.0
epsilonTy = 2.0
epsilonTheta = DEGREE_TO_RADIAN_FACTOR

points_image_modified = []

points_image_compare = []

img_modified = None
img_ori = None


def mouse_clicked_ori(event, x, y, flags, param):
    global points_image_ori

    if event == cv2.EVENT_LBUTTONDOWN:
        points_image_ori.append([x, y, 1])



def get_error(calculated_points):
    global points_image_ori

    A = np.array(
        [[1, 0, -points_image_ori[0][0]*sin(theta)-points_image_ori[0][1]*cos(theta)],
         [0, 1, points_image_ori[0][0]*cos(theta)-points_image_ori[0][1]*sin(theta)],
         [1, 0, -points_image_ori[1][0]*sin(theta)-points_image_ori[1][1]*cos(theta)],
         [0, 1, points_image_ori[1][0]*cos(theta)-points_image_ori[1][1]*sin(theta)],
         [1, 0, -points_image_ori[2][0]*sin(theta)-points_image_ori[2][1]*cos(theta)],
         [0, 1, points_image_ori[2][0]*cos(theta)-points_image_ori[2][1]*sin(theta)]
         ]
    )

    b = np.array([points_image_modified[0][0] - calculated_points[0][0],
                  points_image_modified[0][1] - calculated_points[0][1],
                  points_image_modified[1][0] - calculated_points[1][0],
                  points_image_modified[1][1] - calculated_points[1][1],
                  points_image_modified[2][0] - calculated_points[2][0],
                  points_image_modified[2][1] - calculated_points[2][1]
                  ])


    errorTx, errorTy, errorTheta = np.linalg.lstsq(A,b)[0]

    return errorTx, errorTy, errorTheta


def get_error_szeliski(calculated_points):
    global points_image_ori

    J0 = np.array([
        [1, 0, -sin(theta)*points_image_ori[0][0] - cos(theta)*points_image_ori[0][1] ],
        [0, 1,  cos(theta)*points_image_ori[0][0] - sin(theta)*points_image_ori[0][1] ]
    ])

    A0 = np.dot(J0.T, J0)

    J1 = np.array([
        [1, 0, -sin(theta) * points_image_ori[1][0] - cos(theta) * points_image_ori[1][1]],
        [0, 1,  cos(theta) * points_image_ori[1][0] - sin(theta) * points_image_ori[1][1]]
    ])

    A1 = np.dot(J1.T, J1)

    J2 = np.array([
        [1, 0, -sin(theta) * points_image_ori[2][0] - cos(theta) * points_image_ori[2][1]],
        [0, 1,  cos(theta) * points_image_ori[2][0] - sin(theta) * points_image_ori[2][1]]
    ])

    A2 = np.dot(J2.T, J2)

    A = np.add(np.add(A0, A1), A2)

    deltaX0 = np.array(
        [
            [points_image_modified[0][0] - calculated_points[0][0]],
            [points_image_modified[0][1] - calculated_points[0][1]]
        ]
    )

    deltaX1 = np.array(
        [
            [points_image_modified[1][0] - calculated_points[1][0]],
            [points_image_modified[1][1] - calculated_points[1][1]]
        ]
    )

    deltaX2 = np.array(
        [
            [points_image_modified[2][0] - calculated_points[2][0]],
            [points_image_modified[2][1] - calculated_points[2][1]]
        ]
    )

    b0 = np.dot(J0.T, deltaX0)
    b1 = np.dot(J1.T, deltaX1)
    b2 = np.dot(J2.T, deltaX2)

    b = np.add(np.add(b0, b1),b2)

    errorTx, errorTy, errorTheta = np.linalg.lstsq(A,b)[0]

    return errorTx, errorTy, errorTheta


def calculate_error():
    global tx
    global ty
    global theta
    global points_image_ori

    # get calculated locations
    calculated_points = []
    transformation_matrix = [[cos(theta), -sin(theta),tx],
                             [sin(theta), cos(theta), ty],
                             [0, 0, 1]]
    for p in points_image_ori:
        calculated_point = np.dot(transformation_matrix, p)
        calculated_points.append(calculated_point)

    # return get_error(calculated_points)
    return get_error_szeliski(calculated_points)


def mouse_clicked_modified(event, x, y, flags, param):
    global points_image_modified
    global epsilonTx
    global epsilonTy
    global epsilonTheta
    global tx
    global ty
    global theta
    global deltaTheta, deltaTx, deltaTy

    counter = 0

    if event == cv2.EVENT_LBUTTONDOWN:
        points_image_modified.append([x, y, 1])

        if len(points_image_modified) >= 3:
            eTx, eTy, eTheta = calculate_error()

            print "initial ==> theta: ", theta, " tx: ", tx, " ty: ", ty, " error_theta: ", eTheta, " error_tx: ", eTx, " error_ty: ", eTy

            while abs(eTx) > epsilonTx or abs(eTy) > epsilonTy or abs(eTheta) > epsilonTheta:
                counter += 1
                if counter > 100:
                    break

                theta += eTheta
                tx += eTx
                ty += eTy

                print "iterating ==> theta: ", theta, " tx: ", tx, " ty: ", ty, " error_theta: ", eTheta, " error_tx: ", eTx, " error_ty: ", eTy

                eTx, eTy, eTheta = calculate_error()


            print "final ==> theta: ", theta, " tx: ", tx, " ty: ", ty

            redrawImg()


def redrawImg():
    global img_ori
    global theta, tx, ty

    (rows, cols) = img_ori.shape[:2]

    M = np.float32([[1, 0, tx], [0, 1, ty]])
    dst = cv2.warpAffine(img_ori, M, (cols, rows))

    cv2.imwrite("/tmp/translate.jpg", dst)

    M2 = cv2.getRotationMatrix2D((tx, ty), -theta / DEGREE_TO_RADIAN_FACTOR, 1.)
    dst2 = cv2.warpAffine(dst, M2, (cols, rows))

    cv2.imwrite("/tmp/rotate.jpg", dst2)

    result = cv2.addWeighted(img_modified, 0.7, dst2, 0.3, 0)
    cv2.imwrite("/tmp/result_merged.jpg", result)

    cv2.imshow("RESULT - Images Merged", result)


# img_ori = cv2.imread("resources/mountains_1.jpg")
# cv2.imshow('ORIGINAL IMAGE', img_ori)
# cv2.setMouseCallback("ORIGINAL IMAGE", mouse_clicked_ori)
#
#
# img_modified = cv2.imread("resources/mountains_2.jpg")
# cv2.imshow('MODIFIED IMAGE', img_modified)
# cv2.setMouseCallback("MODIFIED IMAGE", mouse_clicked_modified)
img_ori = cv2.imread("resources/house_orii.jpg")
cv2.imshow('ORIGINAL IMAGE', img_ori)
cv2.setMouseCallback("ORIGINAL IMAGE", mouse_clicked_ori)


img_modified = cv2.imread("resources/house_modifiedd.jpg")
cv2.imshow('MODIFIED IMAGE', img_modified)
cv2.setMouseCallback("MODIFIED IMAGE", mouse_clicked_modified)


cv2.waitKey(0)
cv2.destroyAllWindows()
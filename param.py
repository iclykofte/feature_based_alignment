import numpy as np
from math import sin, cos
import cv2

tx = 154.04
ty = -79.55
theta = 0.52

DEGREE_TO_RADIAN_FACTOR = 0.0174533


img_ori = cv2.imread("resources/mountains_1.jpg")
img_modified = cv2.imread("resources/mountains_2.jpg")


(rows, cols) = img_ori.shape[:2]

img = np.zeros([rows, cols, 3])

for (x, y, z), value in np.ndenumerate(img_modified):
    img[x, y, :] = img_modified[x, y, :]

cv2.imwrite("/tmp/img.jpg", img)

M = np.float32([[1, 0, tx], [0, 1, ty]])
dst = cv2.warpAffine(img_ori, M, (cols, rows))

cv2.imwrite("/tmp/dst.jpg", dst)


M2 = cv2.getRotationMatrix2D((tx, ty), -theta / DEGREE_TO_RADIAN_FACTOR, 1.)
dst2 = cv2.warpAffine(dst, M2, (cols, rows))

cv2.imwrite("/tmp/dst2.jpg", dst2)

result = cv2.addWeighted(img_modified,0.7,dst2,0.3,0)
cv2.imwrite("/tmp/res.jpg", result)

print "Finished!!!"